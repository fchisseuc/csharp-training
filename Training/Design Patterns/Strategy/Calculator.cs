using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Design_Patterns.Strategy
{
    /*
     * Problématique : comment faire pour réaliser différentes opérations avec un seul et même objet 
     * sans violer le principe SOLID "Single Responsibility"
     * 
     * Intent : The intent of the Strategy design pattern helps us to divide an algorithm from a host class 
     * and then move it to another class. By doing so the client can choose which algorithm will be performed 
     * in runtime from a set of algorithms that were implemented earlier.
     * 
     * Sources
     * https://www.c-sharpcorner.com/UploadFile/shinuraj587/strategy-pattern-in-net/
     * https://www.codingame.com/playgrounds/10741/design-pattern-strategy/presentation
     * https://blog.jamesmichaelhickey.com/strategy-pattern-implementations/
     */
    public class Calculator
    {
        public Calculator(ICalculateStrategy calculateStrategy)
        {
            this.CalculateStrategy = calculateStrategy;
        }
        public int Calculate(int val1, int val2)
        {
            return this.CalculateStrategy.Calculate(val1, val2);
        }
        public ICalculateStrategy CalculateStrategy
        {
            get;set;
        }
    }

    public interface ICalculateStrategy
    {
        int Calculate(int val1, int val2);
    }
    public class Plus : ICalculateStrategy
    {
        public int Calculate(int val1, int val2)
        {
            return val1 + val2;
        }
    }
    public class Minus : ICalculateStrategy
    {
        public int Calculate(int val1, int val2)
        {
            return val1 - val2;
        }
    }
}
