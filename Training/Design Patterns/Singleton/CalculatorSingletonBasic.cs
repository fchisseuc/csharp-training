﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Design_Patterns
{
    /* 
     * Objectif : 
     * In singleton design pattern ensures a class has only one instance in the program 
     * and provides a global point of access to it.
     * 
     * Problématique à résoudre : 
     * Certaines applications possèdent des classes qui doivent être instanciées une seule et unique fois.
     * C’est par exemple le cas d’une classe qui implémenterait un pilote pour un périphérique, 
     * ou encore un système de journalisation. En effet, instancier deux fois une classe servant de pilote 
     * à une imprimante provoquerait une surcharge inutile du système et des comportements incohérents.
     * 
     * Sources
     * https://www.c-sharpcorner.com/UploadFile/8911c4/singleton-design-pattern-in-C-Sharp/
     */
    public class CalculatorSingletonBasic
    {
        private static CalculatorSingletonBasic instance;
        private static readonly object o = new object();
        public static CalculatorSingletonBasic Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (o)
                    {
                        if (instance == null)
                            instance = new CalculatorSingletonBasic();
                    }
                }

                return instance;
            }
        }

        public double ValueOne { get; set; }
        public double ValueTwo { get; set; }

        public double Addition()
        {
            return ValueOne + ValueTwo;
        }
    }
}
