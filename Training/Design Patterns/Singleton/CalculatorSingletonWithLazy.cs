﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Design_Patterns.Singleton
{
    public class CalculatorSingletonWithLazy
    {
        private static readonly Lazy<CalculatorSingletonWithLazy> lazy = new Lazy<CalculatorSingletonWithLazy>(
            () => new CalculatorSingletonWithLazy()
        );

        public static CalculatorSingletonWithLazy Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        public double ValueOne { get; set; }
        public double ValueTwo { get; set; }

        public double Addition()
        {
            return ValueOne + ValueTwo;
        }
    }
}
