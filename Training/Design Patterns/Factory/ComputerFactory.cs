﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Design_Patterns.Factory
{
    /*
     * PAttern de type "Creation"
     * Problématique : 
     * Le pattern Factory permet d'encapsuler la création des objets. Ce qui peut être utile lorsque le processus de création est très complexe,
     * Ansi il permet aussi de séparer la création d'objets dérivant d'une classe mère de leur utilisation. 
     * Ce qui permet d'éviter une certaine redondance au niveau de la programmation.
     * PAr ailleurs, on a alors la possibilité de créer plusieurs objets issus d'une même classe mère.
     * 
     * Definition : Factory Pattern lets a class postpone instantiation to sub-classes. 
     * The factory pattern is used to replace class constructors, abstracting the process of object generation 
     * so that the type of the object instantiated can be determined at run-time. 
     * 
     * Sources
     * https://www.c-sharpcorner.com/article/factory-method-design-pattern-in-c-sharp/
     * https://www.codingame.com/playgrounds/36103/design-pattern-factory-abstract-factory/design-pattern-factory
     */
    public abstract class Computer
    {
        public string Cpu { get; set; }
    }

    public class Server : Computer
    {
        public Server(string cpu)
        {
            this.Cpu = cpu;
        }
    }
    public class Pc : Computer
    {
        public Pc(string cpu)
        {
            this.Cpu = cpu;
        }
    }

    public class ComputerFactory
    {
        public static Computer GetComputer(string type, string cpu)
        {
            if (type == "server")
                return new Server(cpu);
            else if (type == "pc")
                return new Pc(cpu);

            return null;
        }
    }
}
