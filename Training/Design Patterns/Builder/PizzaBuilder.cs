﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Design_Patterns.Builder
{
    /*
     * Pattern de type "Creation"
     * Problématique : 
     * Construire un objet ayant plusieurs paramètres sans avoir à les renseigner à chaque fois.
     * 
     * Définition :
     * Le pattern "Builder" permet de construire un objet et de directement définir ses paramètres sans avoir besoin de les renseignés
     * permet de construire un objet complexe step by step, et la dernière step retourne l'objet construit.
     * 
     * Quand utiliser ce pattern :
     * on a besoin de construire un objet en plusieurs étapes (step by step approach)
     * TRES UTILE POUR UNE CLASSE DONT LE CONSTRUCTEUR A PLUSIEURS PARAMETRES
     * 
     * Avantages :
     * meilleure maintenabilité et lisibilité
     * moins de risque d'erreurs car on a une méthode qui retourne l'objet final construit
     * 
     * Inconvénients :
     * plus de lignes de code, mais ça fait sens car meilleure maintenabilité et lisibilité
     * 
     * Sources
     * https://www.c-sharpcorner.com/article/builder-design-pattern-using-c-sharp/
     * https://www.codingame.com/playgrounds/7105/pattern-builder/pattern-builder-2
     */
    public class Pizza
    {
        public string Pate { get; set; }
        public string Sauce { get; set; }
        public string Content { get; set; }
        public string Type { get; set; }
    }

    public interface IPizzaBuilder
    {
        void SetPate();
        void SetSauce();
        void SetContent();
        void SetType();
        Pizza GetPizza();
    }

    public class PizzaHawaienneBuilder : IPizzaBuilder
    {
        Pizza pizza = new Pizza();

        public Pizza GetPizza()
        { 
            return pizza;
        }

        public void SetContent()
        {
            pizza.Content = "jambon et ananas";
        }

        public void SetPate()
        {
            pizza.Pate = "moelleuse";
        }

        public void SetSauce()
        {
            pizza.Sauce = "douce";
        }

        public void SetType()
        {
            pizza.Type = "Hawaienne";
        }
    }

    public class PizzaNorvegienneBuilder : IPizzaBuilder
    {
        Pizza pizza = new Pizza();

        public Pizza GetPizza()
        {
            return pizza;
        }

        public void SetContent()
        {
            pizza.Content = "saumon et mozza";
        }

        public void SetPate()
        {
            pizza.Pate = "cuite";
        }

        public void SetSauce()
        {
            pizza.Sauce = "huile olive";
        }
        public void SetType()
        {
            pizza.Type = "Norvegienne";
        }
    }

    public class PizzaCreator
    {
        IPizzaBuilder PizzaBuilder { get; set; }
        public PizzaCreator(IPizzaBuilder pizzaBuilder)
        {
            PizzaBuilder = pizzaBuilder;
        }
        public Pizza CreatePizza()
        {
            PizzaBuilder.SetPate();
            PizzaBuilder.SetSauce();
            PizzaBuilder.SetContent();
            PizzaBuilder.SetType();

            return PizzaBuilder.GetPizza();
        }
    }
}
