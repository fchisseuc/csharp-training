﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// </summary>
    public class FilterStartsWith
    {
        /// <summary>
        /// Filter devra retourner une énumération ayant les propriétés suivantes :
        /// * les chaines contenues dans l'énumération doivent commencer par la lettre L
        /// * l'énumération doit être triée dans l'ordre alphabétique
        /// </summary>
        /// <param name="strings"></param>
        /// <returns></returns>
        public static IEnumerable<string> Filter(List<string> strings)
        {
            return strings.FindAll(s => s.StartsWith("L")).OrderBy(s => s.ToString());
        }
    }
}
