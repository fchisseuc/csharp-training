﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Objective : get a character from its graphic representation
    /// Implement ScanChar to return the character associated with ASCII character provided
    /// If s does not match a character from A to Z, then return ?
    /// </summary>
    public class AsciiArt
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s">ascii representation of the returned char</param>
        /// <returns></returns>
        public static char ScanChar(string s)
        {
            const char UNMATCHED_ASCII = '?';
            int ascii_number = int.Parse(s);

            //ascii number is unmatched if not between 65 (=A) and 90 (=Z)
            if (ascii_number < 65 || ascii_number > 90)
                return UNMATCHED_ASCII;

            var ascii = new byte[] { byte.Parse(s) };
            for (char c = 'A'; c <= 'Z'; c++)
            {
                //1st solution
                //if (Encoding.ASCII.GetChars(ascii)[0] == c)
                //    return c;

                byte[] b = PrintChar(c);
                //use SequenceEqual to compare byte Array (is slower than comparing each byte with a foreach)
                if (b.SequenceEqual(ascii))
                    return c;
            }

            return UNMATCHED_ASCII;
        }

        static byte[] PrintChar(char c)
        {
            return Encoding.ASCII.GetBytes(c.ToString());
        }
    }
}
