﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// https://app.codility.com/programmers/lessons/1-iterations/binary_gap/
    /// </summary>
    public class BinaryGap
    {
        public int GetBinaryGap(int val)
        {
            //convert to binary string array
            string binaryStr = Convert.ToString(val, 2);

            //Cast binary string array to int array
            //var binaryInt = binaryStr.ToArray().Cast<int>();
            var binaryInt = Array.ConvertAll(binaryStr.ToArray(), s => int.Parse(s.ToString())).ToList();

            //case only "1" ex : 1111 or only one "1" ex : 1000,  => no binary gap
            int countOnes = binaryInt.Count(x => x == 1);
            if (countOnes == 1 || countOnes == binaryInt.Count)
                return 0;

            //case at least two "1"
            var searchItem = 1;
            var startIndex = binaryInt.IndexOf(searchItem);
            var maxBinaryGap = CalculateBinaryGap(binaryInt, searchItem, startIndex);

            //first solution :
            //while (start_index < binaryInt.Count-1)
            //{
            //    int end_index = binaryInt.IndexOf(searchItem, start_index+1);
            //    if (end_index == -1)
            //        break;

            //    //calculate gap
            //    var binaryGap = end_index - start_index - 1;

            //    //compare with previous max gap
            //    //if (binaryGap > maxBinaryGap)
            //    //    maxBinaryGap = binaryGap;
            //    maxBinaryGap = Math.Max(binaryGap, maxBinaryGap);

            //    start_index = end_index;
            //}

            return maxBinaryGap;
        }

        public int CalculateBinaryGap(List<int> list, int searchItem, int startIndex)
        {
            int endIndex = list.IndexOf(searchItem, startIndex + 1);
            if (endIndex == -1)
                return 0;
            var binaryGap = endIndex - startIndex - 1;
            return Math.Max(binaryGap, CalculateBinaryGap(list, searchItem, endIndex));
        }
    }
}
