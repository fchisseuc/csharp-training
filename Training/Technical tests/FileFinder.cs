﻿using System;
using System.IO;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Implémenter une méthode pour retrouver le fichier "universe-formula" et retourner son chemin complet
    /// depuis "/tmp/documents" qui peut contenir des sous-répertoires imbriquées
    /// si "universe-formula" n'existe pas, alors retourner null
    /// Ex : retourne /tmp/documents/a/b/c/universe-formula si universe-formula est trouvé dans le répertoire
    /// /tmp/documents/a/b/c
    /// </summary>
    public class FileFinder
    {
        public static string LocateUniverseFormula()
        {
            var file = Directory.GetFiles(@".\tmp\documents", "universe-formula.txt", SearchOption.AllDirectories);

            return file.Length == 0 ? null : file[0];
        }
    }
}
