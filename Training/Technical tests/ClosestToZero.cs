﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Objective : get the number closest to zero
    /// display zero if no temperature
    /// if 2 numbers are equally close to zero, we should consider the positive one
    /// </summary>
    public class ClosestToZero
    {
        public int GetClosestToZero(string inputsArgs)
        {
            if (string.IsNullOrEmpty(inputsArgs))
                return 0;

            var inputs = inputsArgs.Split(' ');

            //Console.Error.WriteLine("inputs : " + string.Join(" ", inputs));

            /*
            //First solution : 
            long closest_to_zero = int.MaxValue;

            for (int i = 0; i < inputs.Length; i++)
            {
                long t = long.Parse(inputs[i]);
                long current_distance_to_zero = Math.Abs(t);
                long closest_to_zero_distance_to_zero = Math.Abs(closest_to_zero);

                if(current_distance_to_zero < closest_to_zero_distance_to_zero)
                    closest_to_zero = t;
                else if(current_distance_to_zero == closest_to_zero_distance_to_zero)
                    closest_to_zero = Math.Max(t, closest_to_zero);
            }
            */
            
            var min = inputs.Min(i => Math.Abs(long.Parse(i)));

            var closest_to_zero = inputs.Contains(min.ToString()) ? min : -min;

            return (int)closest_to_zero;
        }
    }
}
