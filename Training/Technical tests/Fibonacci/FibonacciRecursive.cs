﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Fibonacci using recursive
    /// This is the WORSE way to implement !!! because :
    /// * time complexity : O(2 power n) => exponential
    /// * space complexity : O(n)
    /// </summary>
    public class FibonacciRecursive
    {
        public static long Fib(int n)
        {
            if (n <= 1)
                return n;

            return Fib(n - 1) + Fib(n - 2);
        }
    }
}
