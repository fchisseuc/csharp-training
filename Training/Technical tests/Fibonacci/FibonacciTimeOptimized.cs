﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Fibonacci using an array (iterative) to store the values up to n
    /// This approach has a better performance on time :
    /// * time complexity : O(n) because sequentially fill the array to store the values
    /// * space complexity : O(n) because the size of the array is n+2
    /// </summary>
    public class FibonacciTimeOptimized
    {
        public static long Fib(int n)
        {
            if (n <= 1)
                return n;

            //declare an array to store fibonacci number up to n
            long[] fib = new long[n+1];
            fib[0] = 0;
            fib[1] = 1;

            for(int i = 2; i<=n; i++)
            {
                fib[i] = fib[i-1] + fib[i-2];
            }

            return fib[n];
        }
    }
}
