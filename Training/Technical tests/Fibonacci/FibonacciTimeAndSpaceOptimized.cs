﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Fibonacci using numbers to store only the 2 previous numbers that we need
    /// This approach has a better performance on time and space :
    /// * time complexity : O(n) because sequentially iterate up to n
    /// * space complexity : O(1) because we use only the size of the 2 numbers
    /// </summary>
    public class FibonacciTimeAndSpaceOptimized
    {
        public static long Fib(int n)
        {
            if (n == 0)
                return 0;

            //initialize previous numbers and fib value for fib(1)
            long previous_n_minus_2;
            long previous_n_minus_1 = 0; //since fib(1-1) = fib(0) = 0
            long fib = 1;   //since fib(1) = 1

            for(int i = 2; i<=n; i++)
            {
                previous_n_minus_2 = previous_n_minus_1;
                previous_n_minus_1 = fib;
                fib = previous_n_minus_1 + previous_n_minus_2;
            }

            return fib;
        }
    }
}
