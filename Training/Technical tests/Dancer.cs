﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Codingame "The dancer" => Finding the position at n
    /// This dance asks every performer to follow a precise sequence of steps:

    ///• Stage 0 : First, get away from obstacles by setting up your starting point at position 0
    ///• Stage 1 : Take one step forward(+1 step)
    ///• Stage 2 : Take two steps backward(-2 steps)

    ///• To follow, the steps as well as the direction you will have to take in your next move will each time be obtained thanks to a specific calculation: the number of steps you took on the previous stage minus the number of steps you took on the penultimate stage (the second last stage).
    ///That is, on stage 3, a dancer will have to take 3 steps backwards(-2 - 1).
    ///At stage 3, the dancer is at position -4.
    ///https://blog.thedevopsnerdworld.com/2018/10/kariakoo-algorithm.html
    ///https://stackoverflow.com/questions/41292387/reccurence-algorithm-find-position-after-n-moves
    ///
    /// Solution :
    /// En calculant manuellement jusqu'au stage 12, on constate que les résultats (nb steps et positions)
    /// obtenus du stage 0 au stage 5, se répètent du stage 6 au stage 11 ... et donc on a un cycle de même résultats tous les 6 stages
    /// la position peut donc être obtenue selon le calcul %6
    /// </summary>
    public class Dancer
    {
        public static readonly int[] POSSIBLE_POSITIONS = { 0, 1, -1, -4, -5, -3 };

        public static int GetPositionAt(int stage)
        {
            return POSSIBLE_POSITIONS[stage % 6];
        }
    }
}
