﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    public class Order
    {
        public string Customer { get; set; }
        public decimal Price { get; set; }
    }
    /// <summary>
    /// </summary>
    public class SuperCustomers
    {
        /// <summary>
        /// GetSuperCustomers devra retourner une énumération de la liste de clients à partir de la liste orders en paramètre
        /// Seuls les "super" clients nous intéresse, sachant qu'un client de ce type est un client qui a dépensé au moiins 100€
        /// Attention, un même client peut avoir effectué plusieurs commandes
        /// </summary>
        /// <param name="strings"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetSuperCustomers(List<Order> orders)
        {
            var groupByCustomer = orders.GroupBy(o => o.Customer);

            var result = groupByCustomer.Where(group => group.Sum(x => x.Price) >= 100);

            return result.Select(g => g.Key);
        }
    }
}
