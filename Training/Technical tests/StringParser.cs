﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// Check string of parenthesis and brackets
    /// you are given a sequence of characters consisting of parenthesis () and brackets []
    /// A string of this type is said to be correct : if it is empty or null, if the sting A is correct.
    /// (A) and [A] are correct if the string A and B are correct, the concatenation AB is also correct
    /// </summary>
    public class StringParser
    {
        public static bool Check(string str)
        {
            if (string.IsNullOrEmpty(str))
                return true;

            var characters_ref = new Dictionary<char, char>();
            characters_ref.Add('(', ')');
            characters_ref.Add('[', ']');

            var characters_stack = new Stack<char>();

            foreach (char c in str)
            {
                if(characters_ref.ContainsKey(c))
                {
                    //open character => push in the stack
                    characters_stack.Push(c);
                }
                else
                {
                    //close character => check if it matches the top open character, and pop
                    var top_open_character = characters_stack.Peek();
                    if (characters_ref[top_open_character] == c)
                        characters_stack.Pop();
                    else
                        return false;
                }
            }
            return characters_stack.Count == 0;
        }
    }
}
