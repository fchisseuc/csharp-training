﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.Technical_Tests
{
    /// <summary>
    /// From the fruit we know the tree
    /// A tree is composed of nodes with the rules :
    /// - a node holds an integer value
    /// - a node has no more that 2 children called left child and right child
    /// - if a node has no right or left child, the corresponding reference is null
    /// - all the descendants to the left are smaller than the node and all the ones to the right are greater
    /// Implement a method Find to return the node holding the value v
    /// If the node does not exist then return null
    /// </summary>
    public class Node
    {
        public int Value { get; set; }
        public Node Parent { get; set; }
        public Node LeftChild { get; set; }
        public Node RightChild { get; set; }

        public Node Find(int v)
        {
            if (v == Value)
                return this;

            if (v < Value && LeftChild != null)
                return LeftChild.Find(v);

            if (v > Value && RightChild != null)
                return RightChild.Find(v);


            return null;
        }
    }
}
