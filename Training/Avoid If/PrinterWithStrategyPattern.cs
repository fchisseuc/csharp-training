﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Training
{
    public class PrinterWithStrategyPattern
    {
        public PrinterWithStrategyPattern(IPrintOrderStrategy printOrderStrategy)
        {
            PrintOrderStrategy = printOrderStrategy;
        }
        public string PrintOrder(Order order)
        {
            if (PrintOrderStrategy == null)
            {
                return "Unknown format"; ;
            }

            return PrintOrderStrategy.PrintOrder(order);
        }
        public IPrintOrderStrategy PrintOrderStrategy
        {
            get; set;
        }
    }

    public interface IPrintOrderStrategy
    {
        string PrintOrder(Order order);
    }

    public class PrinterJson : IPrintOrderStrategy
    {
        public string PrintOrder(Order order)
        {
            return JsonConvert.SerializeObject(order);
        }
    }

    public class PrinterText : IPrintOrderStrategy
    {
        public string PrintOrder(Order order)
        {
            return $"Id: {order.Id}; Client: {order.Client}";
        }
    }
}
