﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Training
{
    public class PrinterWithDictionary
    {
        public string PrintOrder(Order order, string formatType)
        {
            var dict = new Dictionary<string, Func<string>>
            {
                { "Json", () => { return JsonConvert.SerializeObject(order); } },
                { "Text", () => { return $"Id: {order.Id}; Client: {order.Client}"; } }
            };

            if (dict.ContainsKey(formatType))
                return dict[formatType].Invoke();

            return "Unknown format";
        }
    }
}
