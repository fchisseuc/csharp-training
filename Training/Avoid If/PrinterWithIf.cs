﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Training
{
    public class PrinterWithIf
    {
        public string PrintOrder(Order order, string formatType)
        {
            if (formatType == "Json")
            {
                return JsonConvert.SerializeObject(order);
            }
            else if (formatType == "Text")
            {
                return $"Id: {order.Id}; Client: {order.Client}";
            }
            return "Unknown format";
        }
    }
}
