﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DEdgeTechnical;

namespace DEdgeTechnicalTest
{
    [TestFixture]
    public class RepeatingLettersTest
    {
        [Test]
        public void EmptyStringArray()
        {
            var sol = new RepeatingLetters();
            var strings = new string[] { };
            var result = sol.Solution(strings);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void OneStringWithRepeatingLetters()
        {
            var sol = new RepeatingLetters();
            var strings = new string[] { "apple"};
            var result = sol.Solution(strings);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void OneStringWithoutRepeatingLetters()
        {
            var sol = new RepeatingLetters();
            var strings = new string[] { "mango" };
            var result = sol.Solution(strings);
            Assert.AreEqual(5, result);
        }

        [Test]
        public void AllStringsWithoutRepeatingLetters()
        {
            var sol = new RepeatingLetters();
            var strings = new string[] { "co", "di", "ty" };
            var result = sol.Solution(strings);
            Assert.AreEqual(6, result);
        }

        [Test]
        public void SomeStringsWithRepeatingLetters()
        {
            var sol = new RepeatingLetters();
            var strings = new string[] { "co", "dil", "ity" };
            var result = sol.Solution(strings);
            Assert.AreEqual(5, result);
        }
    }
}
