﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DEdgeTechnical;

namespace DEdgeTechnicalTest
{
    [TestFixture]
    public class BicycleParkingTest
    {
        [Test]
        public void OnlyOneRack()
        {
            var sol = new BicycleParkingSequential();
            var positions = new int[] { 1 };
            var result = sol.Solution(positions);
            Assert.AreEqual(0, result);

            positions = new int[] { -1 };
            result = sol.Solution(positions);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void OnlyTwoRacks()
        {
            var sol = new BicycleParking();
            var positions = new int[] { 1, 5};
            var result = sol.Solution(positions);
            Assert.AreEqual(2, result);

            positions = new int[] { -1, -5 };
            result = sol.Solution(positions);
            Assert.AreEqual(2, result);

            positions = new int[] { -1, 5 };
            result = sol.Solution(positions);
            Assert.AreEqual(3, result);

            positions = new int[] { 1, 4 };
            result = sol.Solution(positions);
            Assert.AreEqual(1, result);
        }

        [Test]
        public void MoreThanTwoRacks()
        {
            var sol = new BicycleParking();
            var positions = new int[] { -1, 0, 2, 3, 7 };
            var result = sol.Solution(positions);
            Assert.AreEqual(2, result);

            positions = new int[] { -1, 0, 2, 3, 9 };
            result = sol.Solution(positions);
            Assert.AreEqual(3, result);

            positions = new int[] { -1, 0, 2, 3, 8, 10, 11, 12};
            result = sol.Solution(positions);
            Assert.AreEqual(2, result);
        }
    }
}
