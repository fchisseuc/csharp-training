﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DEdgeTechnical;

namespace DEdgeTechnicalTest
{
    [TestFixture]
    public class MissingIntegerTest
    {
        [Test]
        public void Solution()
        {
            var sol = new MissingInteger();
            var ints = new int[] { 0, 900000 };
            var result = sol.solution(ints);
            Assert.AreEqual(1, result);
        }
    }
}
