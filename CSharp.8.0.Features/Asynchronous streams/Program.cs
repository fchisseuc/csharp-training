﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CSharp._8._0.Features.Async_streams
{
    class Program
    {
        public static async IAsyncEnumerable<int> GenerateSequence()
        {
            for (int i = 0; i < 10; i++)
            {
                await Task.Delay(100);
                yield return i;
            }
        }

        static async Task Main(string[] args)
        {
            await foreach(var number in GenerateSequence())
            {
                Console.WriteLine(number);
            }
        }
        
    }
}
