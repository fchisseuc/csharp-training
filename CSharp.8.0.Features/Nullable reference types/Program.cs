﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp._8._0.Features.Nullable_reference_types
{
    class Program
    {
        static void Main(string[] args)
        {
            string? nullableString = null;
            System.Console.WriteLine(nullableString.Length);

            string notNullableString = null;
            System.Console.WriteLine(notNullableString.Length);
        }
        
    }
}
