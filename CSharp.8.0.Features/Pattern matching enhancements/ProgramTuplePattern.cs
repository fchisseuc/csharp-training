﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp._8._0.Features.Pattern_matching_enhancements
{
    class TuplePattern
    {
        public static string RockPaperScissors(string first, string second) =>
            (first, second) switch
            {
                { first: "rock", second: "paper" } => "paper wins",
                { first:"rock", second: "scissors" } => "rock wins",
                { first: "scissors", second: "paper" } => "scissors wins",
                _ => "tie",
            };
    }

    class ProgramTuplePattern
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                TuplePattern.RockPaperScissors(first: "rock", second: "paper")
            );
            Console.WriteLine(
                TuplePattern.RockPaperScissors(first: "rock", second: "scissors")
            );
            Console.WriteLine(
                TuplePattern.RockPaperScissors(first: "scissors", second: "paper")
            );
            Console.WriteLine(
                TuplePattern.RockPaperScissors(first: "scissors", second: "scissors")
            );
        }

    }
}
