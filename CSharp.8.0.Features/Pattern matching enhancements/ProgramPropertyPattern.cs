﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp._8._0.Features.Pattern_matching_enhancements
{
    public class Address
    {
        public string? Street { get; set; }
        public string? State { get; set; }
    }

    class PropertyPattern
    {
        public static decimal GetTaxFromState(Address location) =>
            location switch
            {
                { State: "Washington" } => 0.06M,
                { State: "New York" } => 0.075M,
                _ => 0M,
            };
    }

    class ProgramPropertyPattern
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                PropertyPattern.GetTaxFromState(new Address() { State = "Washington" })
            );
            Console.WriteLine(
                PropertyPattern.GetTaxFromState(new Address() { State = "New York" })
            );
        }

    }
}
