﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp._8._0.Features.Pattern_matching_enhancements
{
    public enum Colors
    {
        Red,
        Orange,
        Yellow
    }

    class SwitchExpression
    {
        public static string PrintColor(Colors color) =>
            color switch
            {
                Colors.Red => "red",
                Colors.Orange => "ora",
                Colors.Yellow => "yel",
                _ => throw new ArgumentException(message: "invalid enum", paramName:nameof(color)),
            };
    }

    class ProgramSwitchExpression
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                SwitchExpression.PrintColor(Colors.Red)
            );
            Console.WriteLine(
                SwitchExpression.PrintColor(Colors.Orange)
            );
            Console.WriteLine(
                SwitchExpression.PrintColor(Colors.Yellow)
            );
        }

    }
}
