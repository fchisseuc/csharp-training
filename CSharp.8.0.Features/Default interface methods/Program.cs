﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp._8._0.Features.Default_interface_methods
{
    interface IWriteLine
    {
        public void WriteLine()
        {
            Console.WriteLine("Wow C# 8!");
        }
    }

    class WriteLine : IWriteLine
    {
    }

    class Program
    {
        static void Main(string[] args)
        {
            IWriteLine writeline = new WriteLine();
            writeline.WriteLine();
        }
    }
}
