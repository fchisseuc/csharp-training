﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp._8._0.Features.Indices_and_ranges
{
    class Program
    {
        static void Main(string[] args)
        {
            var words = new string[]
            {
                            // index from start    index from end
                "The",      // 0                   ^9
                "quick",    // 1                   ^8
                "brown",    // 2                   ^7
                "fox",      // 3                   ^6
                "jumped",   // 4                   ^5
                "over",     // 5                   ^4
                "the",      // 6                   ^3
                "lazy",     // 7                   ^2
                "dog"       // 8                   ^1
            };              // 9 (or words.Length) ^0

            Console.WriteLine(string.Join(" ", words));

            Console.WriteLine("Indices !");
            Console.WriteLine($"Word on position 3 from beginning : fox == {words[3]}");
            Console.WriteLine($"Word on position 4 from end : over == {words[^4]}");
            Console.WriteLine($"Word on last position : dog == {words[^1]}");

            Console.WriteLine("Ranges (start is inclusive, but the end is exclusive) !");

            var range = words[..];
            Console.WriteLine($"All words : {string.Join(" ", range)}");

            range = words[1..4];
            Console.WriteLine($"Words from positions 1 to 4 (excluded) from beginning : quick brown fox == {string.Join(" ", range)}");

            range = words[^2..^0];
            Console.WriteLine($"Words from positions 2 to 0 (excluded) from end : lazy dog == {string.Join(" ", range)}");

            range = words[..4];
            Console.WriteLine($"Words from begining to position 4 (excluded) from beginning : The quick brown fox == {string.Join(" ", range)}");
            
            range = words[6..];
            Console.WriteLine($"Words from position 6 to the end : the lazy dog == {string.Join(" ", range)}");
        }
    }
}
