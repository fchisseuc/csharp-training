﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Katas.String_Calculator
{
    public class Calculator
    {
        const string DEFAULT_DELIMITER = ",";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
                return 0;

            //split lines
            var lines = numbers.Split("\n");

            //get delimiter
            var delimiter = GetDelimiter(lines);

            //do not consider delimiter row when has delimiter
            string[] lines_without_delimiter = lines;
            if(HasDelimiter(lines))
                lines_without_delimiter = lines[1..];
            
            //get lines of numbers
            var linesNumbers = BuildLinesNumbers(lines_without_delimiter).ToList();

            int result = 0;
            linesNumbers.ForEach(line => result += line.SumLine(delimiter));

            return result;
        }

        private string GetDelimiter(string[] lines)
        {
            if (HasDelimiter(lines))
                return lines[0].Substring(2);

            return DEFAULT_DELIMITER;
        }

        private bool HasDelimiter(string[] lines)
        {
            return lines[0].StartsWith("//");
        }

        private static IEnumerable<LineNumber> BuildLinesNumbers(string[] lines)
        {
            return lines.Select(line => new LineNumber(line));
        }
    }

    public class LineNumber
    {
        public LineNumber(string line)
        {
            Line = line;
        }

        public string Line { get; set; }

        private IEnumerable<int> SplitLine(string delimiter)
        {
            return Line
                    .Split(delimiter)
                    .Select(i => int.Parse(i));
        }

        public int SumLine(string delimiter)
        {
            var numbers = SplitLine(delimiter).ToList(); ;
            int result = 0;
            numbers.ForEach(n => result += n);

            return result;
        }
    }
}
