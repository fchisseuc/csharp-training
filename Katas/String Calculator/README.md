﻿# String Calculator TDD & pair-programming Kata

Sources :
https://kata-log.rocks/string-calculator-kata
https://codingdojo.org/kata/StringCalculator/

Objectives :
* TDD on code from scratch
* Refactoring and incremental implementation following new business rules
* pair programming (one developer per new rule)
