﻿# FizzBuzz Kata | Starter | TDD | Pair-Programming

Sources :
https://kata-log.rocks/fizz-buzz-kata
https://codingdojo.org/kata/FizzBuzz/

Objectifs :
* TDD sur du code from scratch
* Refactoring du code pour éviter la violation des principes SOLID
