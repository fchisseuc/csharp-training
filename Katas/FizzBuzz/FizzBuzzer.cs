﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Katas.FizzBuzz
{
    /// <summary>
    /// Simple FizzBuzz for TDD practice
    /// </summary>
    public class FizzBuzzer
    {
        public string Print(int value)
        {
            if (value < 1 || value > 100)
                throw new ArgumentException("Value should be from 1 to 100", value.ToString());

            if (IsDivisibleBy3(value) && IsDivisibleBy5(value))
                return "FizzBuzz";

            if (IsDivisibleBy3(value))
                return "Fizz";

            if (IsDivisibleBy5(value))
                return "Buzz";

            return value.ToString();
        }

        private static bool IsDivisibleBy3(int value)
        {
            return value % 3 == 0;
        }

        private static bool IsDivisibleBy5(int value)
        {
            return value % 5 == 0;
        }
    }
}
