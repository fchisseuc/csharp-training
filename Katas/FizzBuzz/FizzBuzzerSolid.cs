﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Katas.FizzBuzz
{
    /// <summary>
    /// Improvement of FizzBuzz to :
    /// * avoid violation of Open/Closed Solid principle => Print method should be closed to modification
    /// * apply Strategy pattern to remove If statement for rules
    /// </summary>
    public class FizzBuzzerSolid
    {
        IList<IFizzBuzzRule> FizzBuzzRules = new List<IFizzBuzzRule>();
        public FizzBuzzerSolid()
        {
            var fizzRule = new FizzRule();
            FizzBuzzRules.Add(fizzRule);

            var buzzRule = new BuzzRule();
            FizzBuzzRules.Add(buzzRule);
        }

        public string Print(int value)
        {
            if (value < 1 || value > 100)
                throw new ArgumentException("Value should be from 1 to 100", value.ToString());

            string result = string.Empty;
            foreach(var fizzBuzzRule in FizzBuzzRules)
            {
                if (fizzBuzzRule.Rule(value))
                    result += fizzBuzzRule.Result;
            }

            return string.IsNullOrEmpty(result) ? value.ToString() : result;
        }
    }

    public interface IFizzBuzzRule
    {
        public Func<int, bool> Rule { get; }
        public string Result { get; }
    }

    public class FizzRule : IFizzBuzzRule
    {
        public Func<int, bool> Rule
        {
            get { return (value) => IsDivisibleBy3(value); }
        }
        public string Result
        {
            get { return "Fizz"; }
        }

        private static bool IsDivisibleBy3(int value)
        {
            return value % 3 == 0;
        }
    }

    public class BuzzRule : IFizzBuzzRule
    {
        public Func<int, bool> Rule
        {
            get { return (value) => IsDivisibleBy5(value); }
        }
        public string Result
        {
            get { return "Buzz"; }
        }

        private static bool IsDivisibleBy5(int value)
        {
            return value % 5 == 0;
        }
    }
}
