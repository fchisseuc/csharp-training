﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingTest.Tests
{
    [TestFixture]
    class CollectionTest
    {
        [Test]
        public void TestListBinarySearch()
        {
            var list = new List<int>() { -5, 1, 3, 10 };

            int result = list.BinarySearch(3);

            Assert.AreEqual(2, result); // item 3 should be on index 2
        }

        [Test]
        public void TestSortedSetContains()
        {
            var list = new List<int>() { -5, 1, 3, 10 };
            var set = new SortedSet<int>(list);

            var b = set.Contains(3); //complexity is log(n) and not n as List

            string[] inputs = new string[] { "1", "2" };

            inputs.Min(i => int.Parse(i));

            Assert.AreEqual(true, b);
        }

        [Test]
        public void TestGetMaxElementLength()
        {
            string[] array = new string[] { "pomme", "orange", "abricot", "kiwi"};

            int result = array.Max(c => c.Length);

            Assert.AreEqual(7, result); //it's "abricot" length
        }

        [Test]
        public void TestGetElementWithMaxLength()
        {
            string[] array = new string[] { "pomme", "orange", "abricot", "kiwi" };

            string result = array.OrderByDescending(c => c.Length).First();

            Assert.AreEqual("abricot", result);
        }
    }
}
