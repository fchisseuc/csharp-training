﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingTest.Tests
{
    [TestFixture]
    class DictionaryTest
    {
        [Test]
        public void TestValueChangedWhenKeyAssignedToNewObject()
        {
            var dict = new Dictionary<object, int>();
            var o1 = new object();
            var o2 = o1;
            dict[o1] = 1;
            dict[o2] = 2;
            Assert.AreEqual(2, dict[o1]);
        }

        [Test]
        public void TestValueChangedWhenKeyAssignedToNewString()
        {
            var dict = new Dictionary<string, int>();
            var s1 = "string";
            var s2 = s1;
            dict[s1] = 1;
            dict[s2] = 2;
            Assert.AreEqual(2, dict[s1]);
        }
    }
}
