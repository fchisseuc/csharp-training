﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingTest.Tests
{
    [TestFixture]
    class ArrayTest
    {
        [Test]
        public void TestArray()
        {
            //unidimensional array : initialisations
            int[] uni_array_1 = new int[2];
            int[] uni_array_2 = new int[] { 1, 2};

            //two dimension array : initialisations
            int[,] two_dim_array_1 = new int[1,2];
            int[,] two_dim_array_2 = new int[,] { { 1, 2}, { 3, 5 } };

            //jagged array (array of array) : initialisations
            int[][] jagged_array_1 = new int[2][];
            jagged_array_1[0] = new int[5];
            jagged_array_1[1] = new int[] { 1, 2, 3};

            int[][] jagged_array_2 = new int[][] 
            {
                new int[5],
                new int[] { 1, 2, 3}
            };
        }
    }
}
