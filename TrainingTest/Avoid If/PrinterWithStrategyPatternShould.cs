﻿using NUnit.Framework;
using Training;

namespace TrainingTest
{
    [TestFixture]
    public class PrinterWithStrategyPatternShould
    {
        [Test]
        public void PrintOrderWhenFormatTypeIsJson()
        {
            var order = new Order()
            {
                Id = 1,
                Client = "fch"
            };
            var printManager = new PrinterWithStrategyPattern(new PrinterJson());
            string result = printManager.PrintOrder(order);

            Assert.AreEqual("{\"Id\":1,\"Client\":\"fch\"}", result);
        }

        [Test]
        public void PrintOrderWhenFormatTypeIsText()
        {
            var order = new Order()
            {
                Id = 1,
                Client = "fch"
            };
            var printManager = new PrinterWithStrategyPattern(new PrinterText());
            string result = printManager.PrintOrder(order);

            Assert.AreEqual("Id: 1; Client: fch", result);
        }

        [Test]
        public void PrintOrderWhenFormatTypeIsUnknown()
        {
            var order = new Order()
            {
                Id = 1,
                Client = "fch"
            };
            var printManager = new PrinterWithStrategyPattern(null);
            string result = printManager.PrintOrder(order);

            Assert.AreEqual("Unknown format", result);
        }
    }
}
