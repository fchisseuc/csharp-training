﻿using NUnit.Framework;
using Training;

namespace TrainingTest
{
    [TestFixture]
    public class PrinterWithDictionaryShould
    {
        [Test]
        public void PrintOrderWhenFormatTypeIsJson()
        {
            var order = new Order()
            {
                Id = 1,
                Client = "fch"
            };
            var printManager = new PrinterWithDictionary();
            string result = printManager.PrintOrder(order, "Json");

            Assert.AreEqual("{\"Id\":1,\"Client\":\"fch\"}", result);
        }

        [Test]
        public void PrintOrderWhenFormatTypeIsText()
        {
            var order = new Order()
            {
                Id = 1,
                Client = "fch"
            };
            var printManager = new PrinterWithDictionary();
            string result = printManager.PrintOrder(order, "Text");

            Assert.AreEqual("Id: 1; Client: fch", result);
        }

        [Test]
        public void PrintOrderWhenFormatTypeIsUnknown()
        {
            var order = new Order()
            {
                Id = 1,
                Client = "fch"
            };
            var printManager = new PrinterWithDictionary();
            string result = printManager.PrintOrder(order, "toto");

            Assert.AreEqual("Unknown format", result);
        }
    }
}
