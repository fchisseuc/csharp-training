﻿using NUnit.Framework;
using Training.Design_Patterns.Strategy;

namespace TrainingTest.Design_Patterns.Calculator
{
    [TestFixture]
    public class CalculatorShould
    {
        [Test]
        public void PlusWhenCalculateWithPlusStrategy()
        {
            var calculator = new Training.Design_Patterns.Strategy.Calculator(new Plus());
            Assert.AreEqual(3, calculator.Calculate(2, 1));
        }
        [Test]
        public void MinusWhenCalculateWithMinusStrategy()
        {
            var calculator = new Training.Design_Patterns.Strategy.Calculator(new Minus());
            Assert.AreEqual(1, calculator.Calculate(2, 1));
        }
    }
}
