﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Design_Patterns;

namespace TrainingTest.Design_Patterns.Singleton
{
    [TestFixture]
    public class CalculatorSingletonBasicShould
    {
        [Test]
        public void DoAddition()
        {
            CalculatorSingletonBasic.Instance.ValueOne = 5;
            CalculatorSingletonBasic.Instance.ValueTwo = 10.5;
            Assert.AreEqual(15.5, CalculatorSingletonBasic.Instance.Addition());
            CalculatorSingletonBasic.Instance.ValueTwo = 2;
            Assert.AreEqual(7, CalculatorSingletonBasic.Instance.Addition());
        }
    }
}
