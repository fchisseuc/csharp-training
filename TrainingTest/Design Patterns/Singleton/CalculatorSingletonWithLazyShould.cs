﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Design_Patterns;
using Training.Design_Patterns.Singleton;

namespace TrainingTest.Design_Patterns.Singleton
{
    [TestFixture]
    public class CalculatorSingletonWithLazyShould
    {
        [Test]
        public void DoAddition()
        {
            CalculatorSingletonWithLazy.Instance.ValueOne = 5;
            CalculatorSingletonWithLazy.Instance.ValueTwo = 10.5;
            Assert.AreEqual(15.5, CalculatorSingletonWithLazy.Instance.Addition());
            CalculatorSingletonWithLazy.Instance.ValueTwo = 2;
            Assert.AreEqual(7, CalculatorSingletonWithLazy.Instance.Addition());
        }
    }
}
