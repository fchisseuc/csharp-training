﻿
using NUnit.Framework;
using Training.Design_Patterns.Builder;

namespace TrainingTest.Design_Patterns.Calculator
{
    [TestFixture]
    public class PizzaBuilderShould
    {
        [Test]
        public void CreatePizzaHawaienneWithPizzaHawaienneBuilder()
        {
            var creator = new PizzaCreator(new PizzaHawaienneBuilder());
            var pizza = creator.CreatePizza();
            Assert.AreEqual("Hawaienne", pizza.Type);
        }

        [Test]
        public void CreatePizzaNorvegienneWithPizzaNorvegienneBuilder()
        {
            var creator = new PizzaCreator(new PizzaNorvegienneBuilder());
            var pizza = creator.CreatePizza();
            Assert.AreEqual("Norvegienne", pizza.Type);
        }
    }
}
