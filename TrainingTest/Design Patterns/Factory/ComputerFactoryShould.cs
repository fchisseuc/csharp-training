﻿using NUnit.Framework;
using Training.Design_Patterns.Factory;
using Training.Design_Patterns.Strategy;

namespace TrainingTest.Design_Patterns.Calculator
{
    [TestFixture]
    public class ComputerFactoryShould
    {
        [Test]
        public void GetServerWhenTypeIsServer()
        {
            var computer = ComputerFactory.GetComputer("server", "2Ghz");
            Assert.AreEqual(typeof(Server), computer.GetType());
        }

        [Test]
        public void GetPcWhenTypeIsPc()
        {
            var computer = ComputerFactory.GetComputer("pc", "3Ghz");
            Assert.AreEqual(typeof(Pc), computer.GetType());
        }
    }
}
