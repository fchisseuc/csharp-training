﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;
using FluentAssertions;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class FilterStartsWithShould
    {
        [Test]
        public void Check()
        {
            var list = new List<string>();
            list.Add("Gurt");
            list.Add("Lobster");
            list.Add("Litch");
            list.Add("Doe");

            var listFiltered = FilterStartsWith.Filter(list);
            listFiltered.Should().BeEquivalentTo(new string[] { "Litch", "Lobster" });
        }
    }
}
