﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class DancerShould
    {
        [Test]
        public void Get_position_at_stage_0()
        {
            Assert.AreEqual(0, Dancer.GetPositionAt(0));
        }
        [Test]
        public void Get_position_at_stage_1()
        {
            Assert.AreEqual(1, Dancer.GetPositionAt(1));
        }
        [Test]
        public void Get_position_at_stage_2()
        {
            Assert.AreEqual(-1, Dancer.GetPositionAt(2));
        }
        [Test]
        public void Get_position_at_stage_3()
        {
            Assert.AreEqual(-4, Dancer.GetPositionAt(3));
        }
        [Test]
        public void Get_position_at_stage_100000()
        {
            Assert.AreEqual(-5, Dancer.GetPositionAt(100000));
        }
        [Test]
        public void Get_position_at_stage_maxValue()
        {
            Assert.AreEqual(1, Dancer.GetPositionAt(int.MaxValue));
        }
    }
}
