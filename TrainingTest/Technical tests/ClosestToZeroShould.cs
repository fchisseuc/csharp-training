﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class ClosestToZeroShould
    {
        [Test]
        public void GetClosestToZero()
        {
            var closestToZero = new ClosestToZero();

            Assert.AreEqual(0, closestToZero.GetClosestToZero(""));
            Assert.AreEqual(0, closestToZero.GetClosestToZero(null));
            Assert.AreEqual(1, closestToZero.GetClosestToZero("1 -2 -8 4 5"));
            Assert.AreEqual(2, closestToZero.GetClosestToZero("-5 -4 -2 12 -40 4 2 18 11 5"));
            Assert.AreEqual(-3, closestToZero.GetClosestToZero("-5 -5 -40 -3"));
            Assert.AreEqual(5, closestToZero.GetClosestToZero("-5 5"));
            Assert.AreEqual(int.MaxValue, closestToZero.GetClosestToZero($"{int.MinValue} {int.MaxValue}"));
        }
    }
}
