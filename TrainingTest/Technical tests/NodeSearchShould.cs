﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class NodeSearchShould
    {
        [Test]
        public void Find()
        {
            var node = new Node()
            {
                Value = 9,
                LeftChild = new Node() 
                {
                    Value = 7,
                    LeftChild = new Node() { Value = 5 },
                    RightChild = new Node() { Value = 8 },
                },
                RightChild = new Node() 
                { 
                    Value = 13,
                    RightChild = new Node() { Value = 17 },
                },
            };
            Assert.AreEqual(9, node.Find(9).Value);
            Assert.AreEqual(7, node.Find(7).Value);
            Assert.AreEqual(5, node.Find(5).Value);
            Assert.AreEqual(8, node.Find(8).Value);
            Assert.AreEqual(13, node.Find(13).Value);
            Assert.AreEqual(17, node.Find(17).Value);
            Assert.IsNull(node.Find(20));
        }
    }
}
