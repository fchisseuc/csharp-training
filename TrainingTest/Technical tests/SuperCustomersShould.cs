﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;
using FluentAssertions;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class SuperCustomersShould
    {
        [Test]
        public void Check()
        {
            var list = new List<Order>();
            list.Add(new Order() { Customer = "C3", Price = 125m });
            list.Add(new Order() { Customer = "C2", Price = 150m });
            list.Add(new Order() { Customer = "C1", Price = 50m });
            list.Add(new Order() { Customer = "C1", Price = 25m });
            list.Add(new Order() { Customer = "C4", Price = 10m });
            list.Add(new Order() { Customer = "C4", Price = 190m });

            var result = SuperCustomers.GetSuperCustomers(list);
            result.Should().BeEquivalentTo(new string[] { "C3", "C2", "C4" });
        }
    }
}
