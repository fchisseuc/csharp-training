﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class FibonacciTimeAndSpaceOptimizedShould
    {
        [Test]
        public void CheckBasic()
        {
            Assert.AreEqual(0, FibonacciTimeAndSpaceOptimized.Fib(0));
            Assert.AreEqual(1, FibonacciTimeAndSpaceOptimized.Fib(1));
            Assert.AreEqual(1, FibonacciTimeAndSpaceOptimized.Fib(2));
            Assert.AreEqual(2, FibonacciTimeAndSpaceOptimized.Fib(3));
            Assert.AreEqual(3, FibonacciTimeAndSpaceOptimized.Fib(4));
            Assert.AreEqual(5, FibonacciTimeAndSpaceOptimized.Fib(5));
            Assert.AreEqual(8, FibonacciTimeAndSpaceOptimized.Fib(6));
            Assert.AreEqual(13, FibonacciTimeAndSpaceOptimized.Fib(7));
            Assert.AreEqual(21, FibonacciTimeAndSpaceOptimized.Fib(8));
            Assert.AreEqual(34, FibonacciTimeAndSpaceOptimized.Fib(9));
            Assert.AreEqual(55, FibonacciTimeAndSpaceOptimized.Fib(10));
        }

        [Test, Timeout(2000)]
        public void CheckPerfLessThan2Seconds()
        {
            Assert.AreEqual(2880067194370816120, FibonacciTimeAndSpaceOptimized.Fib(90));
        }
    }
}
