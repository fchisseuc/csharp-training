﻿using NUnit.Framework;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class FibonacciRecursiveShould
    {
        [Test]
        public void CheckBasic()
        {
            Assert.AreEqual(0, FibonacciRecursive.Fib(0));
            Assert.AreEqual(1, FibonacciRecursive.Fib(1));
            Assert.AreEqual(1, FibonacciRecursive.Fib(2));
            Assert.AreEqual(2, FibonacciRecursive.Fib(3));
            Assert.AreEqual(3, FibonacciRecursive.Fib(4));
            Assert.AreEqual(5, FibonacciRecursive.Fib(5));
            Assert.AreEqual(8, FibonacciRecursive.Fib(6));
            Assert.AreEqual(13, FibonacciRecursive.Fib(7));
            Assert.AreEqual(21, FibonacciRecursive.Fib(8));
            Assert.AreEqual(34, FibonacciRecursive.Fib(9));
            Assert.AreEqual(55, FibonacciRecursive.Fib(10));
        }

        [Test, Timeout(2000)]
        public void CheckPerfLessThan2Seconds()
        {
            Assert.AreEqual(2880067194370816120, FibonacciRecursive.Fib(90));
        }
    }
}
