﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class FibonacciTimeOptimizedShould
    {
        [Test]
        public void CheckBasic()
        {
            Assert.AreEqual(0, FibonacciTimeOptimized.Fib(0));
            Assert.AreEqual(1, FibonacciTimeOptimized.Fib(1));
            Assert.AreEqual(1, FibonacciTimeOptimized.Fib(2));
            Assert.AreEqual(2, FibonacciTimeOptimized.Fib(3));
            Assert.AreEqual(3, FibonacciTimeOptimized.Fib(4));
            Assert.AreEqual(5, FibonacciTimeOptimized.Fib(5));
            Assert.AreEqual(8, FibonacciTimeOptimized.Fib(6));
            Assert.AreEqual(13, FibonacciTimeOptimized.Fib(7));
            Assert.AreEqual(21, FibonacciTimeOptimized.Fib(8));
            Assert.AreEqual(34, FibonacciTimeOptimized.Fib(9));
            Assert.AreEqual(55, FibonacciTimeOptimized.Fib(10));
        }

        [Test, Timeout(2000)]
        public void CheckPerfLessThan2Seconds()
        {
            Assert.AreEqual(2880067194370816120, FibonacciTimeOptimized.Fib(90));
        }
    }
}
