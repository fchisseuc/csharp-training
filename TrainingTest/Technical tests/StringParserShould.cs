﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class StringParserShould
    {
        [Test]
        public void Check()
        {
            Assert.AreEqual(true, StringParser.Check(null));
            Assert.AreEqual(true, StringParser.Check(""));
            Assert.AreEqual(true, StringParser.Check("[()]"));
            Assert.AreEqual(true, StringParser.Check("(()[])"));
            Assert.AreEqual(false, StringParser.Check("([)]"));
            Assert.AreEqual(false, StringParser.Check("(("));
            Assert.AreEqual(false, StringParser.Check("[(])]"));
        }
    }
}
