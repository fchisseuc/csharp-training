﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class BinaryGapShould
    {
        [Test]
        public void GetBinaryGap()
        {
            var binaryGap = new BinaryGap();

            Assert.AreEqual(0, binaryGap.GetBinaryGap(32));
            Assert.AreEqual(0, binaryGap.GetBinaryGap(15));
            Assert.AreEqual(2, binaryGap.GetBinaryGap(9));
            Assert.AreEqual(4, binaryGap.GetBinaryGap(529));
            Assert.AreEqual(1, binaryGap.GetBinaryGap(20));
            Assert.AreEqual(5, binaryGap.GetBinaryGap(1041));

        }
    }
}
