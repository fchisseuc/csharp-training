﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Technical_Tests;

namespace TrainingTest.Technical_Tests
{
    [TestFixture]
    public class FileFinderShould
    {
        [Test]
        public void LocateUniverseFormula()
        {
            //change current directory to test folder
            Directory.SetCurrentDirectory(@"D:\DEV\Git\csharp-training\TrainingTest\Technical tests\FileFinder");

            string expected_file_path = @".\tmp\documents\a\b\c\universe-formula.txt";
            Assert.AreEqual(expected_file_path, FileFinder.LocateUniverseFormula());

        }
    }
}
