﻿using NUnit.Framework;
using Katas.FizzBuzz;
using System;

namespace KatasTest.FizzBuzz
{
    [TestFixture]
    public class FizzBuzzerSolidTest
    {
        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(101)]
        public void Should_throw_exception_when_print_with_invalid_number(int value)
        {
            var f = new FizzBuzzerSolid();

            Assert.Throws<ArgumentException>(() => { f.Print(value); });
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(7)]
        public void Should_return_value_when_print_with_valid_number_not_divisible_by_3_or_5(int value)
        {
            var f = new FizzBuzzerSolid();
            Assert.AreEqual(value.ToString(), f.Print(value));
        }

        [TestCase(3)]
        [TestCase(6)]
        [TestCase(9)]
        public void Should_return_Fizz_when_print_with_number_divisible_by_3(int value)
        {
            var f = new FizzBuzzerSolid();
            Assert.AreEqual("Fizz", f.Print(value));
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(20)]
        public void Should_return_Buzz_when_print_with_number_divisible_by_5(int value)
        {
            var f = new FizzBuzzerSolid();
            Assert.AreEqual("Buzz", f.Print(value));
        }

        [TestCase(15)]
        [TestCase(30)]
        [TestCase(60)]
        public void Should_return_FizzBuzz_when_print_with_number_divisible_by_3_and_5(int value)
        {
            var f = new FizzBuzzerSolid();
            Assert.AreEqual("FizzBuzz", f.Print(value));
        }
    }
}
