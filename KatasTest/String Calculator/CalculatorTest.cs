﻿using NUnit.Framework;
using Katas.String_Calculator;
using System;

namespace KatasTest.String_Calculator
{
    [TestFixture]
    public class CalculatorTest
    {
        [TestCase(null)]
        [TestCase("")]
        public void Should_return_0_when_Add_null_or_empty(string numbers)
        {
            var c = new Calculator();
            var result = c.Add(numbers);

            Assert.AreEqual(0, result);
        }

        [Test]
        public void Should_return_1_when_Add_1()
        {
            var c = new Calculator();
            var result = c.Add("1");

            Assert.AreEqual(1, result);
        }

        [Test]
        public void Should_return_3_when_Add_1_and_2()
        {
            var c = new Calculator();
            var result = c.Add("1,2");

            Assert.AreEqual(3, result);
        }

        [Test]
        public void Should_return_6_when_Add_1_and_2_and_3()
        {
            var c = new Calculator();
            var result = c.Add("1,2,3");

            Assert.AreEqual(6, result);
        }

        [Test]
        public void Should_return_11_when_Add_1_and_2_and_3_and_5()
        {
            var c = new Calculator();
            var result = c.Add("1,2,3,5");

            Assert.AreEqual(11, result);
        }

        [Test]
        public void Should_return_6_when_Add_1_newline_and_2_and_3()
        {
            var c = new Calculator();
            var result = c.Add("1\n2,3");

            Assert.AreEqual(6, result);
        }

        [Test]
        public void Should_return_3_when_Add_1_and_2_and_new_separator()
        {
            var c = new Calculator();
            var result = c.Add("//;\n1;2");

            Assert.AreEqual(3, result);
        }
    }
}
