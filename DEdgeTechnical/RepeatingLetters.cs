﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

namespace DEdgeTechnical
{
    public class RepeatingLetters
    {
        public int Solution(string[] A)
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            if (A == null || A.Length == 0)
            {
                return 0;
            }

            if (A.Length == 1 && !HasRepeatingLetters(A[0]))
            {
                return A[0].Length;
            }

            //case repeating letter in all strings => return 0
            if (A.All(s => HasRepeatingLetters(s)))
                return 0;

            //get strings without repetitive letters
            IList<string> listStringWithoutRepetitiveLetters = A.Where(s => !HasRepeatingLetters(s)).ToList();
            IList<string> results = new List<string>();
            string result;

            foreach (string s in listStringWithoutRepetitiveLetters)
            {
                result = GetNonRepetitiveWord(
                    s, listStringWithoutRepetitiveLetters.Where(x => !x.Equals(s)).ToList()
                );
                results.Add(result);
            }

            return results.Max().Count();
        }

        public string GetNonRepetitiveWord(string s, IList<string> list)
        {
            string result = s;
            foreach(string m in list)
            {
                if (!HasRepeatingLetters(s, m))
                    result += m;
            }
            return result;
        }

        public bool HasRepeatingLetters(string s)
        {
            return s.Distinct().Count() != s.Length;
        }

        public bool HasRepeatingLetters(string s1, string s2)
        {
            return s1.Any(c => s2.Contains(c));
        }
    }
}
