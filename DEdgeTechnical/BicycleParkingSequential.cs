﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

namespace DEdgeTechnical
{
    public class BicycleParkingSequential
    {
        public int Solution(int[] A)
        {
            // solution using a sequential search
            var largestDistance = 0;
            Array.Sort(A);
            for (int i = 0; i < A.Length - 1; i++)
            {
                var distance = GetDistance(A[i], A[i + 1]);
                if (distance > largestDistance)
                    largestDistance = distance;
            }

            return largestDistance / 2; //the chosen rack will be in the middle
        }

        private int GetDistance(int pos1, int pos2)
        {
            return Math.Abs(pos1 - pos2);
        }
    }
}
