﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

namespace DEdgeTechnical
{
    public class BicycleParking
    {
        // Subject : https://www.programmersought.com/article/11471762147/
        //https://www.solutioninn.com/index.php?products_id=42165&main_page=student_question_view

        public int Solution(int[] A)
        {
            // solution using dichotomy search with a sorted set

            /* RULES/ASSUMPTIONS
             * array indices => bike ID attached to a rack
             * array value = rack position
             * nb bikes between 2 and 100 000
             * rack position between -1,000,000,000 meters and 1,000,000,000 meters
             * rack aligned in a row
             * a rack can have multiple bikes
             * "used" rack => have bikes attached
             * central position => 0
             * positive rack position x => rack located x meters to the right of the central position
             * negative rack position y => rack located y meters to the left of the central position
             */

            /* GOAL : park your bike in a rack, with the conditions :
             * choosen rack between the first and the last "used" racks
             * return largest distance between choosen rack and any "used" rack
             */

            // Since there is no rule for the case that there is only 1 "used" rack, then
            // we assume that there is at least 2 different "used" racks

            // case only 2 bicycles
            //if(A.Length == 2)
            //    return GetLargestDistanceBetweenPositions(A[0], A[1]);

            // use sorted set to perform dichotomy search since better time complexity than sequential
            var rackPositions = new SortedSet<int>(A);
            var largestDistance = GetLargestDistance(rackPositions);

            //Array.Sort(A);
            //for (int i=0; i<A.Length-1; i++)
            //{
            //    //search for empty position between both positions
            //    if(HasEmptyPositionBetweenPositions(A[i], A[i+1]))
            //        maxLargestDistance = Math.Max(maxLargestDistance, GetLargestDistanceBetweenPositions(A[i], A[i + 1]));
            //}

            return largestDistance / 2; //the chosen rack will be in the middle
        }

        public int GetLargestDistance(SortedSet<int> rackPositions)
        {
            var minRackPos = rackPositions.Min;
            var maxRackPos = rackPositions.Max;

            if (rackPositions.Count == 2)
                return GetDistance(maxRackPos, minRackPos);

            var middleRackPos = rackPositions.ElementAt(rackPositions.Count / 2);
            var distanceLeft = GetDistance(middleRackPos, minRackPos);
            var distanceRight = GetDistance(maxRackPos, middleRackPos);

            SortedSet<int> subRackPositions;
            if (distanceRight > distanceLeft)
                subRackPositions = GetRightRackPositions(rackPositions, middleRackPos);
            else
                subRackPositions = GetLeftRackPositions(rackPositions, middleRackPos);

            return GetLargestDistance(subRackPositions);
        }

        public SortedSet<int> GetLeftRackPositions(SortedSet<int> rackPositions, int referenceRackPosition)
        {
            return rackPositions.GetViewBetween(rackPositions.Min, referenceRackPosition);
        }

        public SortedSet<int> GetRightRackPositions(SortedSet<int> rackPositions, int referenceRackPosition)
        {
            return rackPositions.GetViewBetween(referenceRackPosition, rackPositions.Max);
        }

        private int GetDistance(int pos1, int pos2)
        {
            return Math.Abs(pos1 - pos2);
        }

        //private int GetLargestDistanceBetweenPositions(int pos1, int pos2)
        //{
        //    // we consider that the largest distance between 2 positions is the middle
        //    return GetDistance(pos1, pos2) / 2;
        //}

        //private bool HasEmptyPositionBetweenPositions(int pos1, int pos2)
        //{
        //    return GetDistance(pos1, pos2) > 1;
        //}
    }
}
