﻿using System;
using System.Collections.Generic;
using System.Linq;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

class Solution
{
    public int solution(int[] A)
    {
        // write your code in C# 6.0 with .NET 4.5 (Mono)
        if (A.Count() == 0)
            return 1;

        if (A.Any(i => i > 0))
        {
            int min = A.Min();
            int max = A.Max();
            for (int i = min + 1; i < max; i++)
            {
                if (!A.Contains(i))
                    return i;
            }
            return max + 1;
        }

        return 1;
    }
}
